---
layout: post
title:  "El Origen"
author: john
categories: [ Jekyll, tutorial ]
tags: [red, yellow]
image: assets/images/11.jpg
description: "Inception movie it´s a bad one."
featured: true
hidden: true
rating: 3.5
---

Revise productos, libros, películas, restaurantes y cualquier cosa que desee en su blog de Jekyll con Mediumish! JSON-LD listo para propiedad de revisión..

#### How to use?

It's actually really simple! Add the rating in your YAML front matter. It also supports halfs:

```html
---
layout: post
title:  "Inception Movie"
author: john
categories: [ Jekyll, tutorial ]
tags: [red, yellow]
image: assets/images/11.jpg
description: "My review of Inception movie. Actors, directing and more."
rating: 4.5
---
```
